/*
INPUT:
Số được nhập vào n = 68 

TODO:
Tìm số ở hàng đơn vị 
unit = n % 10 => 68 % 10 = 8
Tìm số ở hàng chục
ten = Math.floor(n%100/10) = Math.floor(68/10) = Math.floor(6.8) = 6
Tổng của 2 kí số
sum = ten + unit;

OUTPUT:
sum = 6 + 8 = 12
 */
var n, unit, ten, sum;
n = 68;
unit = n % 10;
ten = Math.floor(n%100/10);
sum = ten + unit;
console.log("Tổng của 2 kí số là: ", sum);
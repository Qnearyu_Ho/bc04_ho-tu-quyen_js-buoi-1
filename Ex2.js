/*
INPUT: Nhập vào 5 số thực
n1 = 5
n2 = 4
n3 = 3
n4 = 2
n5 = 1

TODO: Tính tổng của 5 số
sum = n1 + n2 + n3 + n4 + n5
Trung bình của 5 số là
result = sum / 5

OUTPUT: Kết quả là 
result = 3
*/ 
var n1, n2, n3, n4, n5, sum, result;
n1 = 5;
n2 = 4;
n3 = 3;
n4 = 2;
n5 = 1;
sum = n1 + n2 + n3 + n4 + n5;
console.log("Sum is: ", sum);
result = sum / 5;
console.log("The average value of five number is: ", result);

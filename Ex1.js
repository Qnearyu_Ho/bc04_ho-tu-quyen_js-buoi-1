/*
INPUT: Lương 1 ngày
Salary_1day = 100000

TODO: Số ngày làm 
n = 30
totalSalary = Salary_1day * n

OUTPUT: Kết quả sau khi tính
Số tiền lương của nhân viên là 100000 * 30 = 3000000

*/ 
var Salary_1day, n, totalSalary;
Salary_1day = 100000;
n = 30;
totalSalary = Salary_1day * n;
console.log("Total salary of worker is: ", totalSalary);
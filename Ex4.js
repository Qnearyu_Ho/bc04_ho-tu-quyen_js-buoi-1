/*
INPUT:
Chiều dài a = 5
Chiều rộng b = 4
TODO:
Diện tích hình chữ nhật S = a * b
Chu vi hình chữ nhật C = (a + b) * 2
OUTPUT: Kết quả
S = 5 * 4 = 20
C = (5 + 4) * 2 = 18
 */
var a, b;
a = 5;
b = 4;
S = a * b;
C = (a + b) * 2;
console.log("Diện tích hình chữ nhật là: ", S);
console.log("Chu vi hình chữ nhật là: ", C);